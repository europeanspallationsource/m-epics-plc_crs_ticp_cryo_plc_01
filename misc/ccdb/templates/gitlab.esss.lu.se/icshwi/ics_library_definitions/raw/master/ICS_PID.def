###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                                 PID - PID Control                                        ##  
##                                                                                          ##
##                                                                                          ##
############################           Version: 1.1           ################################
# Author:	Miklos Boros / Johannes Kazantzidis
# Date:		13-12-2019
# Version:  v1.1
# Changes:  Added alarm signal "Discrepancy_Error"
############################           Version: 1.0           ################################ 
# Author:	Miklos Boros
# Date:		01-09-2019
# Version:  v1.0
# Changes:  First release in TestStand2
##############################################################################################


############################
#  STATUS BLOCK
############################ 
define_status_block()


#Operation modes
add_digital("OpMode_Auto",             PV_DESC="Operation Mode Auto",       PV_ONAM="True",                          PV_ZNAM="False")
add_digital("OpMode_Manual",           PV_DESC="Operation Mode Manual",     PV_ONAM="True",                          PV_ZNAM="False")
add_digital("OpMode_Forced",           PV_DESC="Operation Mode Forced",     PV_ONAM="True",                          PV_ZNAM="False")
add_analog("PID_Color",                "INT",                               PV_DESC="BlockIcon color")


#PID states
add_analog("LMN",                      "REAL",                              PV_DESC="Manipulated Value",             PV_EGU="%")
add_analog("LMN_P",                    "REAL",                              PV_DESC="Manipulated Value P-Action")
add_analog("LMN_I",                    "REAL",                              PV_DESC="Manipulated Value I-Action")
add_analog("LMN_D",                    "REAL",                              PV_DESC="Manipulated Value D-Action")
add_analog("PID_DIF",                  "REAL",                              PV_DESC="Actual PID Error/Difference")
add_analog("PV",                       "REAL",                              PV_DESC="PID Process Value")
add_analog("MAN_SP",                   "REAL",                              PV_DESC="PID Setpoint Value")
add_analog("PID_Cycle",                "INT",                               PV_DESC="PID cycle time",                PV_EGU="ms")
add_string("ProcValueName", 39,         PV_NAME="ProcValueName",            PV_DESC="PV Name of the Process Value")
add_string("ProcValueEGU", 6,           PV_NAME="ProcValueEGU",             PV_DESC="EGU of the Process Value")

#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Manual",          PV_DESC="Inhibit Manual Mode",       PV_ONAM="InhibitManual",                 PV_ZNAM="AllowManual")
add_digital("Inhibit_Force",           PV_DESC="Inhibit Force Mode",        PV_ONAM="InhibitForce",                  PV_ZNAM="AllowForce")
add_digital("Inhibit_Lock",            PV_DESC="Inhibit Locking",           PV_ONAM="InhibitLocking",                PV_ZNAM="AllowLocking")

#Interlock signals
add_digital("MoveInterlock",           PV_DESC="Move Interlock",            PV_ONAM="True",                          PV_ZNAM="False")
add_digital("GroupInterlock",          PV_DESC="Group Interlock",           PV_ONAM="True",                          PV_ZNAM="False")

#for OPI visualization
add_digital("EnableAutoBtn",           PV_DESC="Enable Auto Button",        PV_ONAM="True",                          PV_ZNAM="False")
add_digital("EnableManualBtn",         PV_DESC="Enable Manual Button",      PV_ONAM="True",                          PV_ZNAM="False")
add_digital("EnableForcedBtn",         PV_DESC="Enable Force Button",       PV_ONAM="True",                          PV_ZNAM="False")
add_digital("EnablePIDConf",           PV_DESC="Enable PID Configuration",  PV_ONAM="True",                          PV_ZNAM="False")
add_digital("LatchAlarm",              PV_DESC="Enable Alarm Latching",     PV_ONAM="True",                          PV_ZNAM="False")
add_digital("GroupAlarm",              PV_DESC="Group Alarm for OPI")

#Block Icon controls
add_digital("EnableBlkCtrl",           PV_DESC="Enable Block SP Button",     PV_ONAM="True",             PV_ZNAM="False")

#Forcing
add_digital("EnableForceValBtn",       PV_DESC="Enable Force Value Button", PV_ONAM="True",                          PV_ZNAM="False")

#Locking mechanism
add_digital("DevLocked",               PV_DESC="Device locked",             PV_ONAM="True",                          PV_ZNAM="False")
add_analog("Faceplate_LockID",         "DINT",                              PV_DESC="Owner Lock ID")
add_analog("BlockIcon_LockID",         "DINT",                              PV_DESC="Guest Lock ID")


#Alarm signals
add_digital("LMN_HLIM",                                                     PV_ZNAM="True")
add_digital("LMN_LLIM",                                                     PV_ZNAM="True")
add_major_alarm("Input_Module_Error",  "HW Input Module Error",             PV_ZNAM="NominalState")
add_major_alarm("Output_Module_Error", "HW Output Module Error",            PV_ZNAM="NominalState")
add_major_alarm("Discrepancy_Error",   "Discrepancy_Error",                 PV_ZNAM="NominalState",                  PV_ONAM="Timeout")

#Warning signals
add_minor_alarm("ContDeviceInMan",     "ContDeviceInMan",                   PV_ZNAM="True")

#Feedbacks
#PID
add_analog("FB_Gain",                  "REAL" ,                             PV_DESC="FB Proportional gain")
add_analog("FB_TI",                    "DINT" ,                             PV_DESC="FB Integration time",           PV_EGU="sec")
add_analog("FB_TD",                    "DINT" ,                             PV_DESC="FB Derivative action time",     PV_EGU="sec")

add_analog("FB_DEADB",                 "REAL" ,                             PV_DESC="FB Deadband")
add_analog("FB_LMN_HLIM",              "REAL" ,                             PV_DESC="FB High limit for MV")
add_analog("FB_LMN_LLIM",              "REAL" ,                             PV_DESC="FB Low limit for MV")
add_analog("FB_ForcePosition",         "REAL" ,                             PV_DESC="FB Force Valve Position (AI)",  PV_EGU="%")
add_analog("FB_Setpoint",              "REAL" ,                             PV_DESC="FB Setpoint from HMI (SP)")
add_analog("FB_Step",                  "REAL" ,                             PV_DESC="FB Step value for Open/Close",  PV_EGU="%")
add_analog("FB_Manipulated",           "REAL" ,                             PV_DESC="Force Manipulated value (AO)",  PV_EGU="%")


add_digital("FB_P_ActionState",        PV_DESC="Proportional Action State")
add_digital("FB_I_ActionState",        PV_DESC="Integral Action State")
add_digital("FB_D_ActionState",        PV_DESC="Differential Action State")

#Standby Synchroniztaion
add_digital("FB_SS_State",             PV_DESC="Standby Synchroniztaion State")
add_analog("FB_SS_Value",              "REAL" ,                             PV_DESC="Standby Synchroniztaion Value", PV_EGU="%")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons
add_digital("Cmd_Auto",                PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",              PV_DESC="CMD: Manual Mode")
add_digital("Cmd_Force",               PV_DESC="CMD: Force Mode")

add_digital("Cmd_EnablePID_Conf",      PV_DESC="CMD: Enable PID configuration from OPI")
add_digital("Cmd_DisablePID_Conf",     PV_DESC="CMD: Disable PID configuration from OPI")

add_digital("Cmd_P_ON",                PV_DESC="CMD: Enable P-Action")
add_digital("Cmd_P_OFF",               PV_DESC="CMD: Disable P-Action")

add_digital("Cmd_I_ON",                PV_DESC="CMD: Enable I-Action")
add_digital("Cmd_I_OFF",               PV_DESC="CMD: Disable I-Action")

add_digital("Cmd_D_ON",                PV_DESC="CMD: Enable D-Action")
add_digital("Cmd_D_OFF",               PV_DESC="CMD: Disable D-Action")

add_digital("Cmd_AckAlarm",            PV_DESC="CMD: Acknowledge Alarm")

add_digital("Cmd_ForceValInp",         PV_DESC="CMD: Force Valve PLC Input")
add_digital("Cmd_ForceValOut",         PV_DESC="CMD: Force Valve PLC Output")

add_digital("Cmd_ForceUnlock",         PV_DESC="CMD: Force Unlock Device")
add_digital("Cmd_DevLock",             PV_DESC="CMD: Lock Device")
add_digital("Cmd_DevUnlock",           PV_DESC="CMD: Unlock Device")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()
#PID
add_analog("P_Gain",                   "REAL" ,                             PV_DESC="PID Proportional gain")
add_analog("P_TI",                     "DINT" ,                             PV_DESC="PID Integration time",          PV_EGU="sec")
add_analog("P_TD",                     "DINT" ,                             PV_DESC="PID Derivative action time",    PV_EGU="sec")

add_analog("P_DEADB",                  "REAL" ,                             PV_DESC="PID Deadband")
add_analog("P_LMN_HLIM",               "REAL" ,                             PV_DESC="PID High limit for MV")
add_analog("P_LMN_LLIM",               "REAL" ,                             PV_DESC="PID Low limit for MV")

#Setpoint and Manipulated value from HMI
add_analog("P_ForcePosition",          "REAL" ,                             PV_DESC="Force Valve position (AI)",     PV_EGU="%")
add_analog("P_Setpoint",               "REAL" ,                             PV_DESC="Setpoint from HMI (SP)",        PV_EGU="%")
add_analog("P_Manipulated",            "REAL" ,                             PV_DESC="Force Manipulated value (AO)",  PV_EGU="%")

#Step value when pressing Cmd_Open1Step or Cmd_Close1Step
add_analog("P_Step",                   "REAL" ,                             PV_DESC="Step value for open close")


#Locking mechanism
add_analog("P_Faceplate_LockID",       "DINT",                              PV_DESC="Device ID after Lock")
add_analog("P_BlockIcon_LockID",       "DINT",                              PV_DESC="Device ID after Blockicon Open")
