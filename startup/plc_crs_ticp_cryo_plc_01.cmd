
# @field IPADDR
# @type STRING
# PLC IP address

# @field RECVTIMEOUT
# @type INTEGER
# PLC->EPICS receive timeout (ms), should be longer than frequency of PLC SND block trigger (REQ input)

# @field REQUIRE_plc_crs_ticp_cryo_plc_01_VERSION
# @runtime YES

# @field S7_PORT
# @runtime YES
# Can override S7 port with this

# @field MB_PORT
# @runtime YES
# Can override Modbus port with this

# @field SAVEFILE_DIR
# @type  STRING
# The directory where autosave should save files

# @field REQUIRE_plc_crs_ticp_cryo_plc_01_PATH
# @runtime YES

#- S7 port           : 2000
#- Input block size  : 5750 bytes
#- Output block size : 0 bytes
#- Endianness        : BigEndian
s7plcConfigure("CrS-TICP:Cryo-PLC-01", $(IPADDR), $(S7_PORT=2000), 5750, 0, 1, $(RECVTIMEOUT=300), 0)

#- Modbus port       : 502
drvAsynIPPortConfigure("CrS-TICP:Cryo-PLC-01", $(IPADDR):$(MB_PORT=502), 0, 0, 1)

#- Link type         : TCP/IP (0)
#- The timeout is initialized to the (modbus) default if not specified
modbusInterposeConfig("CrS-TICP:Cryo-PLC-01", 0, $(RECVTIMEOUT=0), 0)

#- Slave address     : 0
#- Function code     : 16 - Write Multiple Registers
#- Addressing        : Absolute (-1)
#- Data segment      : 20 words
drvModbusAsynConfigure("CrS-TICP:Cryo-PLC-01write", "CrS-TICP:Cryo-PLC-01", 0, 16, -1, 20, 0, 0, "S7-1500")

#- Slave address     : 0
#- Function code     : 3 - Read Multiple Registers
#- Addressing        : Relative (0)
#- Data segment      : 10 words
#- Polling           : 1000 msec
drvModbusAsynConfigure("CrS-TICP:Cryo-PLC-01read", "CrS-TICP:Cryo-PLC-01", 0, 3, 0, 10, 0, 1000, "S7-1500")

#- Load plc interface database
dbLoadRecords("plc_crs_ticp_cryo_plc_01.db", "PLCNAME=CrS-TICP:Cryo-PLC-01, MODVERSION=$(REQUIRE_plc_crs_ticp_cryo_plc_01_VERSION), S7_PORT=$(S7_PORT=2000), MODBUS_PORT=$(MB_PORT=502), PAYLOAD_SIZE=5750")

#- Configure autosave
#- Number of sequenced backup files to write
save_restoreSet_NumSeqFiles(1)

#- Specify directories in which to search for request files
set_requestfile_path("$(REQUIRE_plc_crs_ticp_cryo_plc_01_PATH)", "misc")

#- Specify where the save files should be
set_savefile_path("$(SAVEFILE_DIR)", "")

#- Specify what save files should be restored
set_pass0_restoreFile("plc_crs_ticp_cryo_plc_01.sav")

#- Create monitor set
doAfterIocInit("create_monitor_set('plc_crs_ticp_cryo_plc_01.req', 1, '')")
