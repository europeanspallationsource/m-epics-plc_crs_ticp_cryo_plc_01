EXCLUDE_VERSIONS=3.14
include ${EPICS_ENV_PATH}/module.Makefile

AUTO_DEPENDENCIES = NO
USR_DEPENDENCIES += s7plc,1.2.0
USR_DEPENDENCIES += modbus,2.9.0-ESS2
USR_DEPENDENCIES += recsync,1.2.0
MISCS = ${AUTOMISCS} $(addprefix misc/, creator)

USR_DEPENDENCIES += autosave
USR_DEPENDENCIES += synappsstd
MISCS += $(wildcard misc/*.req)
